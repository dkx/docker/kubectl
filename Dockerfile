FROM alpine
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

ARG KUBECTL_VERSION


COPY docker-entrypoint.sh /docker-entrypoint.sh

RUN apk --no-cache add curl ca-certificates && \
	curl -L https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o kubectl && \
	chmod +x kubectl && \
	mv kubectl /usr/local/bin/kubectl && \
	curl -L https://github.com/a8m/envsubst/releases/download/v1.1.0/envsubst-Linux-x86_64 -o envsubst && \
	chmod +x envsubst && \
	mv envsubst /usr/local/bin/envsubst && \
	chmod +x /docker-entrypoint.sh


ENTRYPOINT ["/docker-entrypoint.sh"]
