#!/usr/bin/env sh


set -e


KUBE_NAMESPACE="${KUBE_NAMESPACE:-default}"
KUBE_CA_PEM_FILE="${KUBE_CA_PEM_FILE:-/kubectl-cluster-cert.ca}"


: "${KUBE_URL:?Please set the KUBE_URL environment variable}"
: "${KUBE_TOKEN:?Please set the KUBE_TOKEN environment variable}"


if [[ \( ! -z "${KUBE_CA_PEM}" \) -o \( ! -f "${KUBE_CA_PEM_FILE}" \) ]]; then
	echo "${KUBE_CA_PEM}" > "${KUBE_CA_PEM_FILE}"
fi

if [[ -f "${KUBE_CA_PEM_FILE}" ]]; then
	kubectl config set-cluster dkx/cluster --certificate-authority="${KUBE_CA_PEM_FILE}" --server="${KUBE_URL}" > /dev/null
else
	kubectl config set-cluster dkx/cluster --insecure-skip-tls-verify=true --server="${KUBE_URL}" > /dev/null
	>&2 echo "Running in insecure mode, to fix this set the cluster's certificate file (${KUBE_CA_PEM_FILE})"
fi


kubectl config set-credentials dkx/user --token="${KUBE_TOKEN}" > /dev/null
kubectl config set-context dkx/context --user=dkx/user --cluster=dkx/cluster --namespace="${KUBE_NAMESPACE}" > /dev/null
kubectl config use-context dkx/context > /dev/null


exec "$@"
